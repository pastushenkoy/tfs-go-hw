package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/pkg/errors"
)

func main() {
	var filename string
	flag.StringVar(&filename, "file", "", "Path to input file")
	flag.Parse()
	if filename == "" {
		fmt.Println("No input file specified")
		os.Exit(1)
	}

	candleTypes := []candleType{
		candleType{
			filename:  "candles_5min.csv",
			evaluator: func(timestamp time.Time) time.Time { return timestamp.Truncate(5 * time.Minute) },
		},
		candleType{
			filename:  "candles_30min.csv",
			evaluator: func(timestamp time.Time) time.Time { return timestamp.Truncate(30 * time.Minute) },
		},
		candleType{
			filename: "candles_240min.csv",
			evaluator: func(timestamp time.Time) time.Time {
				return timestamp.Add(-180 * time.Minute).Truncate(240 * time.Minute).Add(180 * time.Minute)
			},
		},
	}

	input := make(chan string)

	done, errc := setupPipeline(input, candleTypes)

	input <- filename
	close(input)

	for {
		select {
		case err := <-errc:
			log.Println(err)
		case <-done:
			os.Exit(0)
		}
	}
}

type candleType struct {
	filename  string
	evaluator func(time.Time) time.Time
}

type trade struct {
	ticker    string
	price     float64
	timestamp time.Time
}

type candle struct {
	ticker   string
	begin    time.Time
	o        float64
	h        float64
	l        float64
	c        float64
	filename string
}

type signal struct{}

func setupPipeline(input chan string, candleTypes []candleType) (done chan signal, errc chan error) {
	done = make(chan signal)

	trades, errcRead := readFile(input)

	candles := buildCandles(trades, candleTypes)

	done, errcWrite := writeToFiles(candles)

	return done, mergeErrors(errcRead, errcWrite)
}

func buildCandles(trades chan trade, candleTypes []candleType) chan candle {
	var creatorInputs []chan trade
	var creatorOutputs []chan candle
	for _, ct := range candleTypes {
		tradesInput := make(chan trade)

		candlesOutput := createCandles(ct, tradesInput)

		creatorInputs = append(creatorInputs, tradesInput)
		creatorOutputs = append(creatorOutputs, candlesOutput)
	}
	forkTrades(trades, creatorInputs)
	return mergeOutputs(creatorOutputs)
}

func readFile(input chan string) (output chan trade, errc chan error) {
	output = make(chan trade)
	errc = make(chan error)
	go func() {
		for fileName := range input {
			csvFile, _ := os.Open(fileName)
			reader := csv.NewReader(bufio.NewReader(csvFile))
			for {
				line, err := reader.Read()
				if err == io.EOF {
					close(output)
					break
				} else if err != nil {
					errc <- errors.Wrap(err, "error reading input file")
					return
				}

				if len(line) < 4 {
					continue
				}

				ticker := line[0]
				price, _ := strconv.ParseFloat(line[1], 64)
				timestamp, _ := time.Parse("2006-01-02 15:04:05.999999", line[3])

				newTrade := trade{
					ticker:    ticker,
					price:     price,
					timestamp: timestamp,
				}

				output <- newTrade
			}
		}
	}()
	return output, errc
}

func createCandles(ct candleType, input chan trade) chan candle {
	output := make(chan candle)

	go func() {
		const tradeStartHour = 7
		const tradeFinishHour = 3

		candlesInProgress := make(map[string]*candle)

		counter := 0
		for t := range input {
			counter++

			if t.timestamp.Hour() < tradeStartHour && t.timestamp.Hour() > tradeFinishHour {
				continue
			}

			intervalBegin := ct.evaluator(t.timestamp)
			currentCandle, ok := candlesInProgress[t.ticker]
			if !ok || currentCandle.begin != intervalBegin {
				if ok {
					output <- *currentCandle
					delete(candlesInProgress, t.ticker)
				}

				candlesInProgress[t.ticker] = &candle{
					ticker:   t.ticker,
					begin:    intervalBegin,
					o:        t.price,
					h:        t.price,
					l:        t.price,
					c:        t.price,
					filename: ct.filename,
				}
			} else {
				if currentCandle.h < t.price {
					currentCandle.h = t.price
				}
				if currentCandle.l > t.price {
					currentCandle.l = t.price
				}
				currentCandle.c = t.price
			}
		}

		for _, c := range candlesInProgress {
			output <- *c
		}

		close(output)
	}()

	return output
}

func writeToFiles(candles <-chan candle) (done chan signal, errc chan error) {
	done = make(chan signal)
	errc = make(chan error)
	go func() {
		createdFiles := make(map[string]*os.File)
		defer func() {
			for _, f := range createdFiles {
				f.Close()
			}
		}()

		var err error

		for c := range candles {
			file, ok := createdFiles[c.filename]
			if !ok {
				file, err = os.Create(c.filename)
				if err != nil {
					errc <- errors.Wrapf(err, "couldn't create file '%s'", c.filename)
					continue
				}
				file, err = os.OpenFile(c.filename, os.O_APPEND|os.O_WRONLY, 0600)
				if err != nil {
					errc <- errors.Wrapf(err, "couldn't open file %s", c.filename)
					continue
				}

				createdFiles[c.filename] = file
			}

			dateStr := c.begin.UTC().Format("2006-01-02T15:04:05Z")
			line := fmt.Sprintf("%s,%s,%.2f,%.2f,%.2f,%.2f\n", c.ticker, dateStr, c.o, c.h, c.l, c.c)

			if _, err := file.WriteString(line); err != nil {
				errc <- errors.Wrapf(err, "couldn't write string to file %s", c.filename)
				continue
			}
		}

		done <- signal{}
	}()

	return done, errc
}

func mergeOutputs(inputs []chan candle) chan candle {
	var wg sync.WaitGroup
	out := make(chan candle)

	output := func(c <-chan candle) {
		for n := range c {
			out <- n
		}
		wg.Done()
	}
	wg.Add(len(inputs))
	for _, in := range inputs {
		go output(in)
	}

	go func() {
		wg.Wait()
		close(out)
	}()
	return out
}

func mergeErrors(inputs ...chan error) chan error {
	var wg sync.WaitGroup
	out := make(chan error)

	output := func(c <-chan error) {
		for n := range c {
			out <- n
		}
		wg.Done()
	}
	wg.Add(len(inputs))
	for _, in := range inputs {
		go output(in)
	}

	go func() {
		wg.Wait()
		close(out)
	}()

	return out
}

func forkTrades(trades <-chan trade, forks []chan trade) {
	go func() {
		for i := range trades {
			for _, fork := range forks {
				fork <- i
			}
		}

		for _, fork := range forks {
			close(fork)
		}
	}()
}
