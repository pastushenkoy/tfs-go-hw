package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"sort"
	"strings"
	"time"
)

func main() {
	var inputFile string
	flag.StringVar(&inputFile, "file", "", "Path to input file")
	flag.Parse()
	if inputFile == "" {
		fmt.Println("No input file specified")
		return
	}

	newsData, err := readFile(inputFile)
	if err != nil {
		fmt.Println("Failed to read and parse file:", err)
		return
	}

	sortedNews := sortNews(newsData)
	groupedNews, err := groupNews(sortedNews)
	if err != nil {
		fmt.Println("Faled to group news:", err)
		return
	}

	orderedNewsGroups := sortNewsGroups(groupedNews)

	err = writeToFile(orderedNewsGroups, "out.json")
	if err != nil {
		fmt.Println("Faled to write result to file:", err)
	}
}

// News is the input structure
type News struct {
	ID          int64     `json:"id"`
	Title       string    `json:"title"`
	Body        string    `json:"body"`
	Provider    string    `json:"provider"`
	PublishedAt time.Time `json:"published_at"`
	Tickers     []string  `json:"tickers"`
	Index       int       `json:"-"`
}

// NewsGroup is the output structure of top level
type NewsGroup struct {
	GroupType   NewsGroupType `json:"type"`
	Payload     interface{}   `json:"payload"`
	PublishedAt time.Time     `json:"-"`
	Index       int           `json:"-"`
}

// NewsGroupType can be "company_news" or "news"
type NewsGroupType string

const (
	groupTypeCompanyNews NewsGroupType = "company_news"
	groupTypeNews        NewsGroupType = "news"
)

// NewsGroupPayload is the output structure to store grouped news
type NewsGroupPayload struct {
	Items       []News    `json:"items"`
	PublishedAt time.Time `json:"published_at"`
	Tickers     []string  `json:"tickers"`
}

type newsGroupKey struct {
	date    time.Time
	tickers string
}

func sortNews(newsData []News) []News {
	sort.SliceStable(newsData, func(i, j int) bool {
		return newsData[i].PublishedAt.Before(newsData[j].PublishedAt)
	})

	return newsData
}

func writeToFile(orderedNewsGroups []NewsGroup, file string) error {
	buf, err := json.MarshalIndent(orderedNewsGroups, "", "    ")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(file, buf, 0644)
	if err != nil {
		return err
	}

	return nil
}

func sortNewsGroups(groupedNews map[newsGroupKey]NewsGroup) []NewsGroup {
	var result []NewsGroup
	for _, value := range groupedNews {
		result = append(result, value)
	}
	sort.Slice(result, func(i, j int) bool {
		if result[i].PublishedAt == result[j].PublishedAt {
			if result[i].GroupType == result[j].GroupType {
				return result[i].Index < result[j].Index
			}
			return result[i].GroupType == groupTypeCompanyNews && result[j].GroupType == groupTypeNews
		}

		return result[i].PublishedAt.Before(result[j].PublishedAt)
	})

	return result
}

func groupNews(newsData []News) (map[newsGroupKey]NewsGroup, error) {
	newsGroups := make(map[newsGroupKey]NewsGroup)
	for _, news := range newsData {
		tickers := getTickersString(news.Tickers)
		date := time.Date(news.PublishedAt.Year(), news.PublishedAt.Month(), news.PublishedAt.Day(), 0, 0, 0, 0, news.PublishedAt.Location())
		key := newsGroupKey{
			date:    date,
			tickers: tickers,
		}

		if group, ok := newsGroups[key]; ok {
			appended, err := appendNewsToGroup(group, news)
			if err != nil {
				return nil, err
			}
			newsGroups[key] = appended
		} else {
			newsGroups[key] = NewsGroup{
				GroupType:   groupTypeNews,
				Payload:     news,
				PublishedAt: news.PublishedAt,
				Index:       news.Index,
			}
		}
	}

	return newsGroups, nil
}

func appendNewsToGroup(group NewsGroup, news News) (NewsGroup, error) {
	if group.GroupType == groupTypeNews {
		firstNews, ok := group.Payload.(News)
		if !ok {
			return NewsGroup{}, fmt.Errorf("Unexpected type: %+v. Expected type: News", group.Payload)
		}
		group.GroupType = groupTypeCompanyNews
		group.Payload = NewsGroupPayload{
			Items:       []News{firstNews, news},
			Tickers:     firstNews.Tickers,
			PublishedAt: firstNews.PublishedAt,
		}
		group.PublishedAt = firstNews.PublishedAt
	} else {
		payload, ok := group.Payload.(NewsGroupPayload)
		if !ok {
			return NewsGroup{}, fmt.Errorf("Unexpected type: %+v. Expected type: NewsGroupPayload", group.Payload)
		}
		payload.Items = append(payload.Items, news)
		group.Payload = payload
	}

	group, err := updateIndex(group)
	if err != nil {
		return NewsGroup{}, err
	}

	return group, nil
}

func updateIndex(group NewsGroup) (NewsGroup, error) {
	group.Index = -1
	payload, ok := group.Payload.(NewsGroupPayload)
	if !ok {
		return NewsGroup{}, fmt.Errorf("Unexpected type: %+v. Expected type: NewsGroupPayload", group.Payload)
	}

	for _, item := range payload.Items {
		if group.Index == -1 || group.Index > item.Index {
			group.Index = item.Index
		}
	}

	return group, nil
}

func getTickersString(tickers []string) string {
	tickersCopy := make([]string, len(tickers))
	copy(tickersCopy, tickers)
	sort.Strings(tickersCopy)
	return strings.Join(tickersCopy, ",")
}

func readFile(inputFile string) (news []News, err error) {
	inputFilePath, err := filepath.Abs(inputFile)
	if err != nil {
		return nil, err
	}

	content, err := ioutil.ReadFile(inputFilePath)
	if err != nil {
		return nil, err
	}

	var newsData []News
	err = json.Unmarshal(content, &newsData)
	if err != nil {
		return nil, err
	}

	for ind := range newsData {
		newsData[ind].Index = ind
	}

	return newsData, nil
}
